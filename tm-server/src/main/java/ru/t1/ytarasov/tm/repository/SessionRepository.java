package ru.t1.ytarasov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.DBConstant;
import ru.t1.ytarasov.tm.api.repository.ISessionRepository;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstant.TABLE_SESSION;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Session fetch(@NotNull ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString(DBConstant.COLUMN_ID));
        session.setRole(Role.valueOf(row.getString(DBConstant.COLUMN_ROLE)));
        session.setDate(row.getTimestamp(DBConstant.COLUMN_DATE));
        session.setUserId(row.getString(DBConstant.COLUMN_USER_ID));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull Session model) {
        @NotNull final String sql = String.format("INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)", getTableName(),
                DBConstant.COLUMN_ID, DBConstant.COLUMN_ROLE, DBConstant.COLUMN_DATE, DBConstant.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getRole().getDisplayName());
            statement.setTimestamp(3, new Timestamp(model.getDate().getTime()));
            statement.setString(4, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session add(@Nullable String userId, @NotNull Session model) {
        model.setUserId(userId);
        return add(model);
    }
}
