package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable Status status) throws Exception;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description, @Nullable Status status) throws Exception;

    @NotNull
    List<Task> findAllTasksByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    @NotNull
    Task update(@NotNull Task task) throws Exception;

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

}
