package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.service.IProjectService;
import ru.t1.ytarasov.tm.api.service.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.ITaskService;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IndexIncorrectException;
import ru.t1.ytarasov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ytarasov.tm.exception.field.TaskIdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public ProjectTaskService(@NotNull IProjectService projectService, @NotNull ITaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskService.update(task);
    }

    @NotNull
    @Override
    public Project removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable List<Task> tasksToRemove = taskService.findAllTasksByProjectId(userId, project.getId());
        if (tasksToRemove == null) throw new TaskNotFoundException();
        for (@NotNull final Task task : tasksToRemove) taskService.removeById(task.getId());
        projectService.removeById(projectId);
        return project;
    }

    @Override
    public void clearAllProjects(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final List<Project> projects = projectService.findAll(userId);
        if (projects == null) return;
        @Nullable final List<Task> tasks = taskService.findAll(userId);
        if (tasks == null) {
            projectService.clear(userId);
            return;
        }
        for (final Project project : projects) {
            for (final Task task : tasks) {
                if (project.getId().equals(task.getProjectId())) taskService.removeById(userId, task.getId());
            }
        }
        projectService.clear(userId);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

}
