package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.ITaskService;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task(name);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.add(userId, task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId,
                       @Nullable final String name,
                       @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.add(userId, task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Override
    public @NotNull Task create(@Nullable final String userId,
                                @Nullable final String name,
                                @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Task task = new Task(name, status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.add(userId, task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId,
                       @Nullable final String name,
                       @Nullable final String description,
                       @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Task task = new Task(name, description, status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.add(userId, task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Override
    public @NotNull List<Task> findAllTasksByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = getRepository(connection);
            return repository.findAllTasksByProjectId(userId, projectId);
        }
    }

    @Override
    @NotNull
    public Task update(@NotNull final Task task) throws Exception {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task updateById(@Nullable final String userId,
                           @Nullable final String id,
                           @Nullable final String name,
                           @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return update(task);
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(@Nullable final String userId,
                                     @Nullable final String id,
                                     @Nullable final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null || status.getDisplayName().isEmpty()) throw new StatusEmptyException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

}
