package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.IUserOwnedRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IUserOwnedService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.exception.entity.ModelNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.IndexIncorrectException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected abstract IUserOwnedRepository<M> getRepository(@NotNull final Connection connection);

    @Override
    public void clear(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        @NotNull boolean exists = false;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            exists = repository.existsById(userId, id);
        } finally {
            connection.close();
        }
        return exists;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<M> models;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            models = repository.findAll(userId);
        } finally {
            connection.close();
        }
        return models;
    }

    @Nullable
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable List<M> models;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            if (comparator == null) models = findAll(userId);
            else models = repository.findAll(userId, comparator);
        } finally {
            connection.close();
        }
        return models;
    }

    @Nullable
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws Exception {
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            model = repository.findOneById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Override
    public int getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        int repositorySize;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repositorySize = repository.getSize(userId);
        } finally {
            connection.close();
        }
        return repositorySize;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId,
                        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return remove(userId, model);
    }

    @Nullable
    @Override
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.add(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId,
                    @Nullable final M model
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.remove(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

}
