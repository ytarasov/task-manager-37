package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable Status status) throws Exception;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description, @Nullable Status status) throws Exception;

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

}