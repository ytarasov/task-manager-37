package ru.t1.ytarasov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.DBConstant;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstant.TABLE_TASK;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString(DBConstant.COLUMN_ID));
        task.setName(row.getString(DBConstant.COLUMN_NAME));
        task.setDescription(row.getString(DBConstant.COLUMN_DESCRIPTION));
        task.setStatus(Status.valueOf(row.getString(DBConstant.COLUMN_STATUS)));
        task.setCreated(row.getTimestamp(DBConstant.COLUMN_CREATED));
        task.setProjectId(row.getString(DBConstant.COLUMN_PROJECT_ID));
        task.setUserId(row.getString(DBConstant.COLUMN_USER_ID));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull Task model) {
        @NotNull final String sql = String.format("INSERT INTO %s(%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstant.COLUMN_ID, DBConstant.COLUMN_NAME, DBConstant.COLUMN_DESCRIPTION,
                DBConstant.COLUMN_STATUS, DBConstant.COLUMN_CREATED, DBConstant.COLUMN_PROJECT_ID, DBConstant.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getName());
            statement.setString(3, model.getDescription());
            statement.setString(4, model.getStatus().getDisplayName());
            statement.setTimestamp(5, new Timestamp(model.getCreated().getTime()));
            statement.setString(6, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public Task add(@Nullable String userId, @NotNull Task model) {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) {
        Task task = new Task(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId,
                       @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        Task task = new Task(name, status);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull String userId, @NotNull String name, @NotNull String description, @NotNull Status status) {
        Task task = new Task(name, description, status);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    @SneakyThrows
    public @Nullable Task update(@NotNull Task task) {
        @NotNull final String name = task.getName();
        @NotNull final String description = task.getDescription();
        @NotNull final Status status = task.getStatus();
        @NotNull final String id = task.getId();
        @Nullable final String projectId = task.getProjectId();
        @Nullable final String userId = task.getUserId();
        if (userId == null) return null;
        @NotNull final String sql = String.format("UPDATE %s SET %s = ?, %s= ?, %s = ?, %s = ? WHERE %s = ? AND %s = ?",
                getTableName(), DBConstant.COLUMN_NAME, DBConstant.COLUMN_DESCRIPTION, DBConstant.COLUMN_STATUS,
                DBConstant.COLUMN_PROJECT_ID, DBConstant.COLUMN_ID, DBConstant.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, name);
            statement.setString(2, description);
            statement.setString(3, status.toString());
            statement.setString(4, projectId);
            statement.setString(5, id);
            statement.setString(6, userId);
            statement.executeUpdate();
            return task;
        }
    }

    @Override
    @SneakyThrows
    public @NotNull List<Task> findAllTasksByProjectId(@NotNull String userId, @NotNull final String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(), DBConstant.COLUMN_PROJECT_ID, DBConstant.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            statement.setString(2, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
            return result;
        }
    }

}
