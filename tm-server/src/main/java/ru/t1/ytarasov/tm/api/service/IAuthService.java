package ru.t1.ytarasov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.Session;
import ru.t1.ytarasov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull User check(@Nullable String login, @Nullable String password) throws Exception;

    void logout(@Nullable final Session session) throws Exception;

    @NotNull
    @SneakyThrows
    Session validateToken(@Nullable String token);

}
