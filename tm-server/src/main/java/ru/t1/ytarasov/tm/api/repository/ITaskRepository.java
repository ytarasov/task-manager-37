package ru.t1.ytarasov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull Status status);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description, @NotNull Status status);

    @Nullable
    Task update(@NotNull Task task);

    @NotNull
    List<Task> findAllTasksByProjectId(@NotNull String userId, @NotNull String projectId);

}
