package ru.t1.ytarasov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ytarasov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.dto.request.task.*;
import ru.t1.ytarasov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ytarasov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ytarasov.tm.dto.response.task.*;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.marker.SoapCategory;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.service.PropertyService;

import java.util.List;
import java.util.UUID;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    private static final String TEST_USER_LOGIN = "TEST";

    @NotNull
    private static final String TEST_USER_PASSWORD = "TEST";

    @NotNull
    private static final String NEW_PROJECT_NAME = "New task";

    @NotNull
    private static final String NEW_PROJECT_DESCRIPTION = "New task";

    @NotNull
    private static final String FAKE_PROJECT_ID = "FAKE";

    @NotNull
    private static final String UPDATE_PROJECT_NAME = "Update task";

    @NotNull
    private static final String UPDATE_PROJECT_DESCRIPTION = "Update task";

    @Nullable
    private String testToken;

    @NotNull
    private final String badToken = UUID.randomUUID().toString();

    @Before
    public void setup() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        testToken = authEndpoint.login(userLoginRequest).getToken();
    }

    @After
    public void tearDown() {
        @NotNull final UserLogoutRequest userLogoutRequest = new UserLogoutRequest(testToken);
        authEndpoint.logout(userLogoutRequest);
    }

    @Test
    public void createTask() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(testToken);
        request.setName(NEW_PROJECT_NAME);
        request.setDescription(NEW_PROJECT_DESCRIPTION);
        @NotNull final TaskCreateRequest badRequest = new TaskCreateRequest();
        badRequest.setName(NEW_PROJECT_NAME);
        badRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @NotNull final TaskCreateRequest badRequest1 = new TaskCreateRequest(badToken);
        badRequest1.setName(NEW_PROJECT_NAME);
        badRequest1.setDescription(NEW_PROJECT_DESCRIPTION);
        @NotNull final TaskCreateRequest badRequest2 = new TaskCreateRequest(badToken);
        badRequest2.setName(null);
        badRequest2.setDescription(NEW_PROJECT_DESCRIPTION);
        @NotNull final TaskCreateRequest badRequest3 = new TaskCreateRequest(badToken);
        badRequest3.setName("");
        badRequest3.setDescription(NEW_PROJECT_DESCRIPTION);
        @NotNull final TaskCreateRequest badRequest4 = new TaskCreateRequest(badToken);
        badRequest4.setName(NEW_PROJECT_NAME);
        badRequest4.setDescription(null);
        @NotNull final TaskCreateRequest badRequest5 = new TaskCreateRequest(badToken);
        badRequest5.setName(NEW_PROJECT_NAME);
        badRequest5.setDescription("");
        @Nullable final TaskCreateResponse response = taskEndpoint.createTask(request);
        Assert.assertNotNull(response);
        @Nullable final Task task = response.getTask();
        Assert.assertNotNull(task);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(badRequest));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(badRequest1));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(badRequest2));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(badRequest3));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(badRequest4));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(badRequest5));
    }

    @Test
    public void listTask() {
        @NotNull final TaskListRequest request = new TaskListRequest(testToken);
        @NotNull final TaskListRequest badRequest = new TaskListRequest(badToken);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(badRequest));
        @Nullable final TaskListResponse response = taskEndpoint.listTask(request);
        Assert.assertNotNull(response);
        @Nullable final List<Task> tasks = response.getTasks();
        Assert.assertNotNull(tasks);
        @NotNull final UserLogoutRequest userLogoutRequest = new UserLogoutRequest(testToken);
        authEndpoint.logout(userLogoutRequest);
    }

    @Test
    public void showTaskById() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(testToken);
        taskCreateRequest.setName(NEW_PROJECT_NAME);
        taskCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Task task = taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(task);
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(testToken);
        request.setTaskId(task.getId());
        @NotNull final TaskShowByIdRequest badRequest = new TaskShowByIdRequest();
        @NotNull final TaskShowByIdRequest badRequest1 = new TaskShowByIdRequest(badToken);
        badRequest1.setTaskId(task.getId());
        @NotNull final TaskShowByIdRequest badRequest2 = new TaskShowByIdRequest(testToken);
        badRequest2.setTaskId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.showTaskById(badRequest));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.showTaskById(badRequest1));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.showTaskById(badRequest2));
        @Nullable final TaskShowByIdResponse response = taskEndpoint.showTaskById(request);
        Assert.assertNotNull(response);
        @Nullable final Task checkTask = response.getTask();
        Assert.assertNotNull(checkTask);
        Assert.assertEquals(task.getId(), checkTask.getId());
    }

    @Test
    public void startTaskById() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(testToken);
        taskCreateRequest.setName(NEW_PROJECT_NAME);
        taskCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Task task = taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(task);
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(testToken);
        request.setTaskId(task.getId());
        @NotNull final TaskStartByIdRequest badRequest = new TaskStartByIdRequest();
        @NotNull final TaskStartByIdRequest badRequest1 = new TaskStartByIdRequest(badToken);
        badRequest1.setTaskId(task.getId());
        @NotNull final TaskStartByIdRequest badRequest2 = new TaskStartByIdRequest(testToken);
        badRequest2.setTaskId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(badRequest));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(badRequest1));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(badRequest2));
        @Nullable final TaskStartByIdResponse response = taskEndpoint.startTaskById(request);
        Assert.assertNotNull(response);
        @Nullable final Task startedTask = response.getTask();
        Assert.assertNotNull(startedTask);
        Assert.assertEquals(Status.IN_PROGRESS.toString(), startedTask.getStatus().toString());
    }

    @Test
    public void completeTaskById() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(testToken);
        taskCreateRequest.setName(NEW_PROJECT_NAME);
        taskCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Task task = taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(task);
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(testToken);
        request.setTaskId(task.getId());
        @NotNull final TaskCompleteByIdRequest badRequest = new TaskCompleteByIdRequest();
        @NotNull final TaskCompleteByIdRequest badRequest1 = new TaskCompleteByIdRequest(badToken);
        @NotNull final TaskCompleteByIdRequest badRequest2 = new TaskCompleteByIdRequest(testToken);
        badRequest2.setTaskId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskById(badRequest));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskById(badRequest1));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskById(badRequest2));
        @Nullable final TaskCompleteByIdResponse response = taskEndpoint.completeTaskById(request);
        Assert.assertNotNull(response);
        @Nullable final Task startedTask = response.getTask();
        Assert.assertNotNull(startedTask);
        Assert.assertEquals(Status.COMPLETED.toString(), startedTask.getStatus().toString());
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(testToken);
        taskCreateRequest.setName(NEW_PROJECT_NAME);
        taskCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Task task = taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(task);
        @NotNull final TaskCompleteByIdRequest taskCompleteByIdRequest = new TaskCompleteByIdRequest(testToken);
        taskCompleteByIdRequest.setTaskId(task.getId());
        @Nullable final Task completedTask = taskEndpoint.completeTaskById(taskCompleteByIdRequest).getTask();
        Assert.assertNotNull(completedTask);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(testToken);
        request.setTaskId(task.getId());
        request.setStatus(Status.IN_PROGRESS);
        @NotNull final TaskChangeStatusByIdRequest badRequest = new TaskChangeStatusByIdRequest();
        @NotNull final TaskChangeStatusByIdRequest badRequest1 = new TaskChangeStatusByIdRequest(badToken);
        @NotNull final TaskChangeStatusByIdRequest badRequest2 = new TaskChangeStatusByIdRequest(testToken);
        badRequest2.setTaskId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(badRequest));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(badRequest1));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(badRequest2));
        @Nullable final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        Assert.assertNotNull(response);
        @Nullable final Task changedStatusTask = response.getTask();
        Assert.assertNotNull(changedStatusTask);
        Assert.assertNotEquals(completedTask.getStatus().toString(), changedStatusTask.getStatus().toString());
    }

    @Test
    public void removeById() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(testToken);
        taskCreateRequest.setName(NEW_PROJECT_NAME);
        taskCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Task task = taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(task);
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(testToken);
        request.setTaskId(task.getId());
        @NotNull final TaskRemoveByIdRequest badRequest = new TaskRemoveByIdRequest();
        @NotNull final TaskRemoveByIdRequest badRequest1 = new TaskRemoveByIdRequest(badToken);
        @NotNull final TaskRemoveByIdRequest badRequest2 = new TaskRemoveByIdRequest(testToken);
        badRequest2.setTaskId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(badRequest));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(badRequest1));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(badRequest2));
        @Nullable final TaskRemoveByIdResponse response = taskEndpoint.removeTaskById(request);
        Assert.assertNotNull(response);
        @Nullable final Task removedTask = response.getTask();
        Assert.assertNotNull(removedTask);
    }

    @Test
    public void updateById() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(testToken);
        taskCreateRequest.setName(NEW_PROJECT_NAME);
        taskCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Task task = taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(task);
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(testToken);
        request.setTaskId(task.getId());
        request.setName(UPDATE_PROJECT_NAME);
        request.setDescription(UPDATE_PROJECT_DESCRIPTION);
        @NotNull final TaskUpdateByIdRequest badRequest = new TaskUpdateByIdRequest();
        @NotNull final TaskUpdateByIdRequest badRequest1 = new TaskUpdateByIdRequest(badToken);
        @NotNull final TaskUpdateByIdRequest badRequest2 = new TaskUpdateByIdRequest(testToken);
        badRequest2.setTaskId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(badRequest));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(badRequest1));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(badRequest2));
        @Nullable final TaskUpdateByIdResponse response = taskEndpoint.updateTaskById(request);
        Assert.assertNotNull(response);
        @Nullable final Task updatedTask = response.getTask();
        Assert.assertNotNull(updatedTask);
    }

}
